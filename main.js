var mysql = require('mysql');

var http = require('http');

var async = require('async');
asyncTest();

http.createServer(function (req, res) {
	res.writeHead(200, {'Content-Type': 'text/html'});
	
	console.log("Url: " + req.url);
	
	var ip = getCallerIP(req); ;
	console.log("IP: " + ip);
	
	process(res);
}).listen(8080);

function asyncTest() {
	async.series([function(callback) {
		console.log("Call 1");
		callback(null);
	},
	function(callback) {
		console.log("Call 2");
		callback(null);
	}], 
	function(err, results) {
		console.log("Done");
	});
}

function process(res) {
	var con = mysql.createConnection({
	  host: "localhost",
	  user: "root",
	  password: "estradelle27",
	  database: "mydb"
	});
	
	con.connect(function(err) {
		if (err) {
			console.log("Error on connect");
			throw err;
		}
	  
		console.log("Connected!");
	  
		var sql = "SELECT * FROM test";
		
		async.series([function(callback) {
			con.query(sql, function (err, result, fields) {
				if (err) {
					console.log("Error on query");
					throw err;
				}
				
				for(var i = 0; i < result.length; ++i) {
					var row = result[i];
					var id = row.id;
					var name = row.name;
					var address = row.address;
					
					res.write("first id: " + id + ", name: " + name + ", address: " + address + "<br>");
				}
				
				callback(null);
			});
		},
		function(callback) {
			con.query(sql, function (err, result, fields) {
				if (err) {
					console.log("Error on query");
					throw err;
				}
				
				for(var i = 0; i < result.length; ++i) {
					var row = result[i];
					var id = row.id;
					var name = row.name;
					var address = row.address;
					
					res.write("second id: " + id + ", name: " + name + ", address: " + address + "<br>");
				}
				
				callback(null);
			});
		}], 
		function(err, results) {
			res.end("End");
		});
	});
}

function getCallerIP(request) {
    var ip = request.headers['x-forwarded-for'] ||
        request.connection.remoteAddress ||
        request.socket.remoteAddress ||
        request.connection.socket.remoteAddress;
    ip = ip.split(',')[0];
    ip = ip.split(':').slice(-1); //in case the ip returned in a format: "::ffff:146.xxx.xxx.xxx"
    return ip;
}
